#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>
#include "sr_arpcache.h"
#include "sr_router.h"
#include "sr_if.h"
#include "sr_protocol.h"
#include "sr_rt.h"
#include "sr_utils.h"

/* 
  This function gets called every second. For each request sent out, we keep
  checking whether we should resend an request or destroy the arp request.
  See the comments in the header file for an idea of what it should look like.
*/
void sr_arpcache_sweepreqs(struct sr_instance *sr) { 
    /* Fill this in */
    pthread_mutex_lock(&((&(sr->cache))->lock));

    struct sr_arpreq *current_request = sr->cache.requests;
	struct sr_arpreq *request_tmp;
	
	while (current_request != NULL){
		request_tmp = current_request->next;
		handle_arpreq(sr, current_request);
		current_request = request_tmp;
	}
	pthread_mutex_unlock(&((&(sr->cache))->lock));
}

/**
 *    #Sending packet to next_hop_ip
 */
void send_packet_by_checking_arp(struct sr_instance *sr, uint8_t* packet,
	uint32_t next_hop_ip, unsigned int packet_len, char* des_iface_name) {

	pthread_mutex_lock(&((&(sr->cache))->lock));

	struct sr_arpentry * entry = sr_arpcache_lookup(&(sr->cache), next_hop_ip);
	
	if (entry) {
		/*copy mac to packet first 8 bytes*/
		memcpy(packet, entry->mac, ETHER_ADDR_LEN); 
		memcpy(packet+ETHER_ADDR_LEN, (sr_get_interface(sr, des_iface_name))->addr, ETHER_ADDR_LEN); 

		sr_send_packet(sr, packet, packet_len, des_iface_name);
		free(entry);
	}
	else {

		struct sr_arpreq * req = sr_arpcache_queuereq(&(sr->cache),
					next_hop_ip, packet, packet_len, des_iface_name);
		handle_arpreq(sr, req);
	}
	pthread_mutex_unlock(&((&(sr->cache))->lock));
}

/**
 *  Handle ARP request
 * The handle_arpreq() function is a function you should write, and it should
 * handle sending ARP requests if necessary:
 */
void handle_arpreq(struct sr_instance *sr, struct sr_arpreq * req) {
	pthread_mutex_lock(&((&(sr->cache))->lock));

	time_t current_time = time(0);
	
    if (difftime(current_time, req->sent) >= 1.0) {
		if (req->times_sent >= 5){
			/*send icmp host unreachable to source addr of all pkts 
			 *waiting on this request*/
			struct sr_packet * current_packet = req->packets;
			while (current_packet != NULL){
				sr_ip_hdr_t* org_ip_hdr = (sr_ip_hdr_t *)(current_packet->buf + sizeof(sr_ethernet_hdr_t));
				struct sr_rt* rt_entry = get_longest_prefix_match(sr, org_ip_hdr->ip_src);
				if (rt_entry == NULL){
					printf("Couldn't find the suource network in routing table\n");
					return;
				}
				struct sr_rt* rt_entry_dest = get_longest_prefix_match(sr, org_ip_hdr->ip_dst);
				if (rt_entry_dest == NULL){
					printf("Couldn't find the suource network in routing table\n");
					return;
				}
				send_icmp_packet(sr, current_packet->buf, rt_entry->interface, sr_get_interface(sr, rt_entry_dest->interface)->ip, rt_entry->gw.s_addr, 3, 1);
				current_packet = current_packet->next;
			}
			sr_arpreq_destroy(&(sr->cache), req);
		}
		else {
			/*send arp request*/
			send_arp_request_packet(sr, (req->packets)->iface, req->ip);
			req->sent = current_time;
			req->times_sent++;
		}
	}
	pthread_mutex_unlock(&((&(sr->cache))->lock));
}

/**
 *    The ARP reply processing code should move entries from the ARP request
   queue to the ARP cache:

   # When servicing an arp reply that gives us an IP->MAC mapping
   req = arpcache_insert(ip, mac)

   if req:
       send all packets on the req->packets linked list
       arpreq_destroy(req)
 */
void handle_arpreply(struct sr_instance *sr, uint32_t ip, unsigned char *mac, 
					 const char* des_iface_name) {
	pthread_mutex_lock(&((&(sr->cache))->lock));

	struct sr_arpreq * req = sr_arpcache_insert(&(sr->cache), mac, ip);
	
	if (req){
		struct sr_packet * current_packet = req->packets;
		while (current_packet != NULL){
			/*copy mac to current packet first 8 bytes*/
			/*copy destination mac*/
			memcpy(current_packet->buf, mac, ETHER_ADDR_LEN); 
			printf("this is my \"handle_arpreply\", and my des_iface_name is : %s\n", des_iface_name);
			print_hdrs(current_packet->buf, current_packet->len);
			/*copy source mac*/
			memcpy(current_packet->buf+ETHER_ADDR_LEN, (sr_get_interface(sr, des_iface_name))->addr, ETHER_ADDR_LEN); 

			sr_send_packet(sr, current_packet->buf, current_packet->len, des_iface_name);
			current_packet = current_packet->next;
		}
		sr_arpreq_destroy(&(sr->cache), req);
	}
	pthread_mutex_unlock(&((&(sr->cache))->lock));
}

/* You should not need to touch the rest of this code. */

/* Checks if an IP->MAC mapping is in the cache. IP is in network byte order.
   You must free the returned structure if it is not NULL. */
struct sr_arpentry *sr_arpcache_lookup(struct sr_arpcache *cache, uint32_t ip) {
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpentry *entry = NULL, *copy = NULL;
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        if ((cache->entries[i].valid) && (cache->entries[i].ip == ip)) {
            entry = &(cache->entries[i]);
        }
    }
    
    /* Must return a copy b/c another thread could jump in and modify
       table after we return. */
    if (entry) {
        copy = (struct sr_arpentry *) malloc(sizeof(struct sr_arpentry));
        memcpy(copy, entry, sizeof(struct sr_arpentry));
    }
        
    pthread_mutex_unlock(&(cache->lock));
    
    return copy;
}

/* Adds an ARP request to the ARP request queue. If the request is already on
   the queue, adds the packet to the linked list of packets for this sr_arpreq
   that corresponds to this ARP request. You should free the passed *packet.
   
   A pointer to the ARP request is returned; it should not be freed. The caller
   can remove the ARP request from the queue by calling sr_arpreq_destroy. */
struct sr_arpreq *sr_arpcache_queuereq(struct sr_arpcache *cache,
                                       uint32_t ip,
                                       uint8_t *packet,           /* borrowed */
                                       unsigned int packet_len,
                                       char *iface)
{
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpreq *req;
    for (req = cache->requests; req != NULL; req = req->next) {
        if (req->ip == ip) {
            break;
        }
    }
    
    /* If the IP wasn't found, add it */
    if (!req) {
        req = (struct sr_arpreq *) calloc(1, sizeof(struct sr_arpreq));
        req->ip = ip;
        req->next = cache->requests;
        cache->requests = req;
    }
    
    /* Add the packet to the list of packets for this request */
    if (packet && packet_len && iface) {
        struct sr_packet *new_pkt = (struct sr_packet *)malloc(sizeof(struct sr_packet));
        
        new_pkt->buf = (uint8_t *)malloc(packet_len);
        memcpy(new_pkt->buf, packet, packet_len);
        new_pkt->len = packet_len;
		new_pkt->iface = (char *)malloc(sr_IFACE_NAMELEN);
        strncpy(new_pkt->iface, iface, sr_IFACE_NAMELEN);
        new_pkt->next = req->packets;
        req->packets = new_pkt;
    }
    
    pthread_mutex_unlock(&(cache->lock));
    
    return req;
}

/* This method performs two functions:
   1) Looks up this IP in the request queue. If it is found, returns a pointer
      to the sr_arpreq with this IP. Otherwise, returns NULL.
   2) Inserts this IP to MAC mapping in the cache, and marks it valid. */
struct sr_arpreq *sr_arpcache_insert(struct sr_arpcache *cache,
                                     unsigned char *mac,
                                     uint32_t ip)
{
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpreq *req, *prev = NULL, *next = NULL; 
    for (req = cache->requests; req != NULL; req = req->next) {
        if (req->ip == ip) {            
            if (prev) {
                next = req->next;
                prev->next = next;
            } 
            else {
                next = req->next;
                cache->requests = next;
            }
            
            break;
        }
        prev = req;
    }
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        if (!(cache->entries[i].valid))
            break;
    }
    
    if (i != SR_ARPCACHE_SZ) {
        memcpy(cache->entries[i].mac, mac, 6);
        cache->entries[i].ip = ip;
        cache->entries[i].added = time(NULL);
        cache->entries[i].valid = 1;
    }
    
    pthread_mutex_unlock(&(cache->lock));
    
    return req;
}

/* Frees all memory associated with this arp request entry. If this arp request
   entry is on the arp request queue, it is removed from the queue. */
void sr_arpreq_destroy(struct sr_arpcache *cache, struct sr_arpreq *entry) {
    pthread_mutex_lock(&(cache->lock));
    
    if (entry) {
        struct sr_arpreq *req, *prev = NULL, *next = NULL; 
        for (req = cache->requests; req != NULL; req = req->next) {
            if (req == entry) {                
                if (prev) {
                    next = req->next;
                    prev->next = next;
                } 
                else {
                    next = req->next;
                    cache->requests = next;
                }
                
                break;
            }
            prev = req;
        }
        
        struct sr_packet *pkt, *nxt;
        
        for (pkt = entry->packets; pkt; pkt = nxt) {
            nxt = pkt->next;
            if (pkt->buf)
                free(pkt->buf);
            if (pkt->iface)
                free(pkt->iface);
            free(pkt);
        }
        
        free(entry);
    }
    
    pthread_mutex_unlock(&(cache->lock));
}

/* Prints out the ARP table. */
void sr_arpcache_dump(struct sr_arpcache *cache) {
    fprintf(stderr, "\nMAC            IP         ADDED                      VALID\n");
    fprintf(stderr, "-----------------------------------------------------------\n");
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        struct sr_arpentry *cur = &(cache->entries[i]);
        unsigned char *mac = cur->mac;
        fprintf(stderr, "%.1x%.1x%.1x%.1x%.1x%.1x   %.8x   %.24s   %d\n", 
				mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], 
				ntohl(cur->ip), ctime(&(cur->added)), cur->valid);
    }
    
    fprintf(stderr, "\n");
}

/* Initialize table + table lock. Returns 0 on success. */
int sr_arpcache_init(struct sr_arpcache *cache) {  
    /* Seed RNG to kick out a random entry if all entries full. */
    srand(time(NULL));
    
    /* Invalidate all entries */
    memset(cache->entries, 0, sizeof(cache->entries));
    cache->requests = NULL;
    
    /* Acquire mutex lock */
    pthread_mutexattr_init(&(cache->attr));
    pthread_mutexattr_settype(&(cache->attr), PTHREAD_MUTEX_RECURSIVE);
    int success = pthread_mutex_init(&(cache->lock), &(cache->attr));
    
    return success;
}

/* Destroys table + table lock. Returns 0 on success. */
int sr_arpcache_destroy(struct sr_arpcache *cache) {
    return pthread_mutex_destroy(&(cache->lock)) 
	&& pthread_mutexattr_destroy(&(cache->attr));
}

/* Thread which sweeps through the cache and invalidates entries that were added
   more than SR_ARPCACHE_TO seconds ago. */
void *sr_arpcache_timeout(void *sr_ptr) {
    struct sr_instance *sr = sr_ptr;
    struct sr_arpcache *cache = &(sr->cache);
    
    while (1) {
        sleep(1.0);
        
        pthread_mutex_lock(&(cache->lock));
    
        time_t curtime = time(NULL);
        
        int i;    
        for (i = 0; i < SR_ARPCACHE_SZ; i++) {
            if ((cache->entries[i].valid) && 
				(difftime(curtime,cache->entries[i].added) > SR_ARPCACHE_TO)) {
                cache->entries[i].valid = 0;
            }
        }
        
        sr_arpcache_sweepreqs(sr);

        pthread_mutex_unlock(&(cache->lock));
    }
    
    return NULL;
}
