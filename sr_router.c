﻿/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>
#include "stdlib.h"
#include <string.h>

#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

	/* NAT *//*AAA*/
    if (sr->nat_mode) {
        sr_nat_init(&(sr->nat));
    }

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
    
    /* Add initialization code here! */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

 void sr_handlepacket(struct sr_instance* sr,
	uint8_t * packet/* lent */,
	unsigned int len,
	char* interface/* lent */){
	/* REQUIRES */
	assert(sr);
	assert(packet);
	assert(interface);
	printf("*** -> Received packet of length %d\n", len);

	/*AAA*/
	struct sr_if* in_iface = sr_get_interface(sr, interface); /*packet come in from this interface*/
    struct sr_ethernet_hdr* ether_hdr = (struct sr_ethernet_hdr*)packet;
 
	/* Ethernet Frame Validation */
	int minlength = sizeof(sr_ethernet_hdr_t);
	if(len < minlength){
		return;
	}
		
	int packet_type = ethertype(packet);
	switch(packet_type){
		case ethertype_arp:
			sr_handle_arp(sr, packet, len, interface);
			break;
		case ethertype_ip:
			/*sr_handle_ip(sr, packet, len, interface);*/
			if (sr->nat_mode == 1) {
				Debug("NAT mode activated");
				sr_nat_handle_ip(sr, &(sr->nat), packet, len, in_iface, ether_hdr);
			}
			else {
				Debug("NAT mode inactive; run normal simple router");
				sr_handle_ip(sr, packet, len, interface);
			}
			break;
	}
	/*print_hdrs(packet, len);*/
	printf("========================================\n\n\n\n\n\n");
 }/* end sr_ForwardPacket */

void sr_handle_arp(struct sr_instance* sr, uint8_t * packet, 
					unsigned int len, char* interface){
	sr_arp_hdr_t* org_arp = (sr_arp_hdr_t *)(packet + sizeof(sr_ethernet_hdr_t));

	/* ARP Packet Validation */
	int minlength = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
	if(len < minlength){
		return;
	}
	if(org_arp->ar_tip != sr_get_interface(sr, interface)->ip){
		printf("this is \"org_arp->ar_tip\": %d\n",org_arp->ar_tip);
		printf("this is \"interface ip\": %d\n",sr_get_interface(sr, interface)->ip);
		printf("This is not my packet!\n");
		return;
	}
  
	printf("I Received an ARP Packet, ");   
	if(ntohs(org_arp->ar_op) == arp_op_request){ /* COMPLETE */
		printf("this is an ARP Request:\n");
		print_hdrs(packet, len);
		send_arp_reply_packet(sr, packet, len, interface);
	}else if(ntohs(org_arp->ar_op) == arp_op_reply){
		printf("this is an ARP Reply:\n");
		print_hdrs(packet, len);
		handle_arpreply(sr, org_arp->ar_sip, org_arp->ar_sha, interface);
	}else{
		printf("\t This is an invalid type ARQ\n");
	}
}

void sr_handle_ip(struct sr_instance* sr, uint8_t * packet, 
					unsigned int len, char* interface){
	printf("I Received an IP Packet!\n");
	print_hdrs(packet, len);	
	sr_ip_hdr_t* org_ip_hdr = (sr_ip_hdr_t *)(packet + sizeof(sr_ethernet_hdr_t));
	struct sr_rt* rt_entry = get_longest_prefix_match(sr, org_ip_hdr->ip_dst);
	struct sr_if* if_entry = sr_get_interface_with_ip(sr, org_ip_hdr->ip_dst);
	printf("Destination Iface: %s \n", if_entry->name);

	/* IP Packet Validation */
	int minlength = sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t);
	if(len < minlength){
		return;
	}
	
	uint16_t ip_hdr_exp_cksum;
    uint16_t ip_hdr_rev_cksum;
    ip_hdr_rev_cksum = org_ip_hdr->ip_sum;
    org_ip_hdr->ip_sum = 0;
    ip_hdr_exp_cksum = cksum(org_ip_hdr, sizeof(sr_ip_hdr_t));

    if (ip_hdr_exp_cksum != ip_hdr_rev_cksum) {
		printf("ERROR: IP packet checksum. Dropping. \n");
		return;
	}
	org_ip_hdr->ip_sum = ip_hdr_rev_cksum;
	
	
	if(if_entry != NULL){ /* This Packet is for me! $_$ */
		rt_entry = get_longest_prefix_match(sr, org_ip_hdr->ip_src);
		if (rt_entry == NULL){
				printf("Couldn't find the suource network in routing table\n");
				return;
		}
		if(org_ip_hdr->ip_p == ip_protocol_icmp){
			printf("This is an icmp packet for me!\n");
			struct sr_icmp_hdr* icmp_hdr= (struct sr_icmp_hdr*)(packet + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr));
			uint16_t icmp_exp_cksum;
            uint16_t icmp_rev_cksum;

            icmp_rev_cksum = icmp_hdr->icmp_sum;
            icmp_hdr->icmp_sum = 0;
            icmp_exp_cksum = cksum(icmp_hdr, len- sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t));

            if (icmp_rev_cksum != icmp_exp_cksum)
            {
                printf("ERROR: ICMP packet checksum. Dropping. \n");
                return;
            }
            icmp_hdr->icmp_sum = icmp_rev_cksum;
            /*use des ip as source ip*/
			send_icmp_packet_t0(sr, packet, len, rt_entry->interface, org_ip_hdr->ip_dst, rt_entry->gw.s_addr, 0, 0);
		} else {
			/*TODO: TCP/UDP */
			printf("This is TCP/UDP packet for me!\n");
			send_icmp_packet(sr, packet, rt_entry->interface, org_ip_hdr->ip_dst, rt_entry->gw.s_addr, 3, 3);
		}
	}else{/* This packet is not for me :( */
		if (org_ip_hdr->ip_ttl <= 1){
			rt_entry = get_longest_prefix_match(sr, org_ip_hdr->ip_src);
			if (rt_entry == NULL){
				printf("Couldn't find the suource network in routing table\n");
				return;
			}
			send_icmp_packet(sr, packet, rt_entry->interface, sr_get_interface(sr, rt_entry->interface)->ip, rt_entry->gw.s_addr, 11, 0);
			return;
		}
		/*update TTL */
		int ip_hdr_size = sizeof(sr_ip_hdr_t); /*20*/
		org_ip_hdr->ip_ttl--;
		org_ip_hdr->ip_sum = 0;
		org_ip_hdr->ip_sum = cksum(org_ip_hdr, ip_hdr_size);
		
		if(rt_entry == NULL){
			/* The destination IP is not in our routing table */
			printf("The destination IP is not in our routing table!\n");
			rt_entry = get_longest_prefix_match(sr, org_ip_hdr->ip_src);
			if (rt_entry == NULL){
				printf("Couldn't find the suource network in routing table\n");
				return;
			}
			printf("This is interface name: %s\n", rt_entry->interface);
			/*use source ip's gate way as source ip*/
			send_icmp_packet(sr, packet, rt_entry->interface, sr_get_interface(sr, rt_entry->interface)->ip, rt_entry->gw.s_addr, 3, 0);
		}else{
			printf("\n=========> Dest ip addr found in the routing table. \n");
			/*des ip as souce ip*/
			send_packet_by_checking_arp(sr, packet, rt_entry->gw.s_addr, len, rt_entry->interface);
		}
	}
  
  /* sr_print_routing_table(sr); */
  /* print_hdrs(packet, len); */
  
}

void send_arp_reply_packet(struct sr_instance* sr, uint8_t * packet, 
          unsigned int len, char* interface){
	/* print_hdrs(packet, len); */
	sr_ethernet_hdr_t* org_ether = (sr_ethernet_hdr_t *)packet;
	sr_arp_hdr_t* org_arp = (sr_arp_hdr_t *)(packet + sizeof(sr_ethernet_hdr_t));
	struct sr_if* pck_intf = sr_get_interface(sr, interface);
    
	/* Creat ARP Header */
	sr_arp_hdr_t* reply_arp = (sr_arp_hdr_t*)malloc(sizeof(sr_arp_hdr_t));
	reply_arp->ar_hrd = htons(arp_hrd_ethernet);     /* format of hardware address   */
	reply_arp->ar_pro = htons(ethertype_ip);         /* format of protocol address   */
	reply_arp->ar_hln = ETHER_ADDR_LEN;              /* length of hardware address   */
	reply_arp->ar_pln = org_arp->ar_pln;             /* length of protocol address   */
	reply_arp->ar_op = ntohs(arp_op_reply);          /* ARP opcode (command)         */
	memcpy(reply_arp->ar_sha, pck_intf->addr, ETHER_ADDR_LEN);
	reply_arp->ar_sip = org_arp->ar_tip;
	memcpy(reply_arp->ar_tha, org_arp->ar_sha, ETHER_ADDR_LEN);
	reply_arp->ar_tip = org_arp->ar_sip;
	/* print_hdr_arp((uint8_t *)reply_arp); */
    
    /* Create Ethernet Header */
    sr_ethernet_hdr_t* reply_ether = (sr_ethernet_hdr_t*)malloc(sizeof(sr_ethernet_hdr_t));
    memcpy(reply_ether->ether_dhost, org_ether->ether_shost, ETHER_ADDR_LEN);  /* destination ethernet address */
    memcpy(reply_ether->ether_shost, pck_intf->addr, ETHER_ADDR_LEN);      /* source ethernet address */
    reply_ether->ether_type = htons(ethertype_arp);
    /* print_hdr_eth((uint8_t *)reply_ether);*/
    
    /* Combine them Together */
    int total_size = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
    uint8_t* reply_packet = (uint8_t*)malloc(total_size);
    memcpy(reply_packet, reply_ether, sizeof(sr_ethernet_hdr_t));
	memcpy(reply_packet + sizeof(sr_ethernet_hdr_t), reply_arp, sizeof(sr_arp_hdr_t));
	print_hdrs(reply_packet, total_size);
	sr_send_packet(sr, reply_packet, total_size, pck_intf->name);
	/*send_packet_by_checking_arp(sr, reply_packet, org_arp->ar_sip, total_size, pck_intf->name);*/
	/* Free Allocated Spaces */
	free(reply_arp);
	free(reply_ether);
	free(reply_packet);
}

void send_arp_request_packet(struct sr_instance* sr, 
					const char* interface_name, uint32_t dest_ip){
	/* Assume I can always get an interface */
	struct sr_if* if_entry = sr_get_interface(sr, interface_name);			
	sr_print_if(if_entry);

	/* Create Ethernet Header */
	sr_ethernet_hdr_t* request_ether = (sr_ethernet_hdr_t*)malloc(sizeof(sr_ethernet_hdr_t));
	memcpy(request_ether->ether_shost, if_entry->addr, ETHER_ADDR_LEN);
	int i;
	for(i=0; i<ETHER_ADDR_LEN; i++)
		request_ether->ether_dhost[i] = 255;
	request_ether->ether_type=htons(ethertype_arp);

	/* Create ARP Header */
	sr_arp_hdr_t* request_arp = (sr_arp_hdr_t*)malloc(sizeof(sr_arp_hdr_t));
	request_arp->ar_hrd = htons(arp_hrd_ethernet);     /* Hardware Type               */
	request_arp->ar_pro = htons(ethertype_ip);         /* Protocal Type               */
	request_arp->ar_hln = ETHER_ADDR_LEN;              /* length of hardware address  */
	request_arp->ar_pln = 4;             			   /* length of protocol address  */
	request_arp->ar_op = htons(arp_op_request);        /* ARP opcode (command)        */
				
	memcpy(request_arp->ar_sha, if_entry->addr, ETHER_ADDR_LEN);
	request_arp->ar_sip = if_entry->ip;
	for(i=0; i<ETHER_ADDR_LEN; i++)
		request_arp->ar_tha[i] = 255; 
	request_arp->ar_tip = dest_ip;

	/* Combine them Together */
	int total_size = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
	uint8_t* request_packet = (uint8_t*)malloc(total_size);
	memcpy(request_packet, request_ether, sizeof(sr_ethernet_hdr_t));
	memcpy(request_packet + sizeof(sr_ethernet_hdr_t), request_arp, sizeof(sr_arp_hdr_t));
	printf("this is my \"send_arp_request_packet\" and if_entry->name is : %s\n",if_entry->name);
	print_hdrs(request_packet,total_size);
	sr_send_packet(sr, request_packet, total_size, if_entry->name);

	/* Free Allocated Spaces */
	free(request_ether);
	free(request_arp);
	free(request_packet);
}

void send_icmp_packet_t0(struct sr_instance* sr, uint8_t * packet, unsigned int len, 
		char* interface, uint32_t src_ip, uint32_t next_hop_ip, int in_icmp_type, int in_icmp_code){
	/*Here I will assume the packet is a valid packet*/

    printf("\n\n === Construct and send icmp packet \n");
    int ether_hdr_size = sizeof(sr_ethernet_hdr_t);/*14*/
    int ip_hdr_size = sizeof(sr_ip_hdr_t); /*20*/
    int icmp_hdr_size = sizeof(sr_icmp_hdr_t); /*4*/
	int icmp_whole_size = len - ether_hdr_size - ip_hdr_size; 
    int total_size = ether_hdr_size + ip_hdr_size + icmp_whole_size;
    struct sr_if* intf = sr_get_interface(sr, interface);

    /* Create Ethernet Header */
    sr_ethernet_hdr_t* org_ether = (sr_ethernet_hdr_t *)packet;
    sr_ethernet_hdr_t* icmp_ether_hdr = 
		create_ether_hdr(org_ether->ether_shost, intf->addr, ethertype_ip);
		
	/* Create IP Header*/
    sr_ip_hdr_t* org_ip = (sr_ip_hdr_t *)(packet + ether_hdr_size);
	sr_ip_hdr_t* icmp_ip_hdr = (sr_ip_hdr_t*)malloc(ip_hdr_size);
	/* memcpy(icmp_ip_hdr, packet + ether_hdr_size, ip_hdr_size);*/
	icmp_ip_hdr->ip_v = 4;
	icmp_ip_hdr->ip_hl = ip_hdr_size/4; /* (ip_hdr_size * 8)/32 = 5 */
	icmp_ip_hdr->ip_tos = 0;
	icmp_ip_hdr->ip_len = htons(ip_hdr_size + icmp_whole_size);
	icmp_ip_hdr->ip_id = org_ip->ip_id; 
	icmp_ip_hdr->ip_off = htons(0b0100000000000000);  /* flags = 010, offset = 0 */
	icmp_ip_hdr->ip_ttl = 64; 
	icmp_ip_hdr->ip_p = ip_protocol_icmp; 		
	icmp_ip_hdr->ip_dst = org_ip->ip_src;
	icmp_ip_hdr->ip_src = src_ip;
	icmp_ip_hdr->ip_sum = 0;
	icmp_ip_hdr->ip_sum = cksum(icmp_ip_hdr, ip_hdr_size);
	print_hdr_ip((uint8_t *)icmp_ip_hdr); 
	
	/* Create ICMP Header*/	
	printf("ICMP DATA d should be 64 :%d \n",icmp_whole_size);
	uint8_t* icmp_data = (uint8_t*)malloc(icmp_whole_size);
	sr_icmp_hdr_t* icmp_hdr = (sr_icmp_hdr_t*)icmp_data;
	
	icmp_hdr->icmp_type = in_icmp_type;
	icmp_hdr->icmp_code = in_icmp_code;
    icmp_hdr->icmp_sum = 0;

	/*AAA*/
	sr_icmp_hdr_t* icmp_from_packet = (sr_icmp_hdr_t*)(packet + ether_hdr_size + ip_hdr_size);
	icmp_hdr->icmp_iden = icmp_from_packet->icmp_iden;
	icmp_hdr->icmp_seqn = icmp_from_packet->icmp_seqn;

	memcpy(icmp_data + icmp_hdr_size, packet + ether_hdr_size + ip_hdr_size + icmp_hdr_size, icmp_whole_size - icmp_hdr_size);
	
    icmp_hdr->icmp_sum = cksum(icmp_data, icmp_whole_size);
	print_hdr_icmp((uint8_t *)icmp_data);
	printf("ICMP DATA DONE============\n\n");
	
	/* Combine them Together */
	uint8_t* icmp_packet = (uint8_t*)malloc(total_size);
	memcpy(icmp_packet, icmp_ether_hdr, ether_hdr_size);
	memcpy(icmp_packet + ether_hdr_size, icmp_ip_hdr, ip_hdr_size);
	memcpy(icmp_packet + ether_hdr_size + ip_hdr_size, icmp_data, icmp_whole_size);
	
	printf("This is the interface in: %s\n", interface);
	print_hdrs(icmp_packet,total_size);
	/*sr_send_packet(sr, icmp_packet, total_size, interface);*/
	send_packet_by_checking_arp(sr, icmp_packet, next_hop_ip, total_size, interface);
		
	free(icmp_ether_hdr);		
	free(icmp_ip_hdr);
	free(icmp_data);
	free(icmp_packet);
}


void send_icmp_packet(struct sr_instance* sr, uint8_t * packet, 
		char* interface, uint32_t src_ip, uint32_t next_hop_ip, int in_icmp_type, int in_icmp_code){
	/* Here I will assume the packet is a valid packet*/

    printf("\n\n === Construct and send icmp packet \n");
    int ether_hdr_size = sizeof(sr_ethernet_hdr_t);/*14*/
    int ip_hdr_size = sizeof(sr_ip_hdr_t); /*20*/
    int icmp_hdr_size = sizeof(sr_icmp_t3_hdr_t); /*36*/
    int hdr_total_size = ether_hdr_size + ip_hdr_size + icmp_hdr_size;
    sr_ethernet_hdr_t* org_ether = (sr_ethernet_hdr_t *)packet;
    sr_ip_hdr_t* org_ip_hdr = (sr_ip_hdr_t*)(packet + ether_hdr_size);
    struct sr_if* intf = sr_get_interface(sr, interface);

    /* sr_arp_hdr_t* org_arp = (sr_arp_hdr_t *)(packet + sizeof(sr_ethernet_hdr_t)); */

    /* Create Ethernet Header */
    sr_ethernet_hdr_t* icmp_ether_hdr = 
		create_ether_hdr(org_ether->ether_shost, intf->addr, ethertype_ip);
		
	/* Create IP Header*/
	sr_ip_hdr_t* icmp_ip_hdr = (sr_ip_hdr_t*)malloc(ip_hdr_size);
	icmp_ip_hdr->ip_v = 4;
    icmp_ip_hdr->ip_hl = ip_hdr_size/4;
    icmp_ip_hdr->ip_tos = 0;
    icmp_ip_hdr->ip_len = htons(ip_hdr_size + icmp_hdr_size);
    icmp_ip_hdr->ip_id = org_ip_hdr->ip_id; 
    icmp_ip_hdr->ip_off = htons(0b0100000000000000);  /*offset*/
    icmp_ip_hdr->ip_ttl = 64;              /*TTL*/
    icmp_ip_hdr->ip_p = ip_protocol_icmp;  /*protocol*/
	icmp_ip_hdr->ip_dst = org_ip_hdr->ip_src; /*destination ip address*/
	icmp_ip_hdr->ip_src = src_ip;
	icmp_ip_hdr->ip_sum = 0;
	icmp_ip_hdr->ip_sum = cksum(icmp_ip_hdr, ip_hdr_size);
	
	/* Create ICMP Header*/
	sr_icmp_t3_hdr_t* icmp_hdr;
	icmp_hdr = (sr_icmp_t3_hdr_t*)malloc(icmp_hdr_size);
	icmp_hdr->icmp_type = in_icmp_type; /* Destination Unreachabl */
	icmp_hdr->icmp_code = in_icmp_code; /* Destination network unreachable*/
	icmp_hdr->icmp_sum = 0;
	icmp_hdr->unused = 0;
	icmp_hdr->next_mtu = 0;
	print_hdr_ip((uint8_t *)packet + ether_hdr_size);
	print_hdr_icmp(packet + ether_hdr_size + ip_hdr_size);
	memcpy(icmp_hdr->data, packet + ether_hdr_size, ICMP_DATA_SIZE);
	icmp_hdr->icmp_sum = cksum(icmp_hdr, icmp_hdr_size);
	
	/* Combine them Together */
	uint8_t* icmp_packet = (uint8_t*)malloc(hdr_total_size);
	memcpy(icmp_packet, icmp_ether_hdr, sizeof(sr_ethernet_hdr_t));
	memcpy(icmp_packet + ether_hdr_size, icmp_ip_hdr, ip_hdr_size);
	memcpy(icmp_packet + ether_hdr_size + ip_hdr_size, icmp_hdr, sizeof(sr_icmp_t3_hdr_t));
	
	printf("This is the interface in: %s\n", interface);
	print_hdrs(icmp_packet,hdr_total_size);
	/*sr_send_packet(sr, icmp_packet, hdr_total_size, interface);*/
	send_packet_by_checking_arp(sr, icmp_packet, next_hop_ip, hdr_total_size, interface);
		
	free(icmp_hdr);
	free(icmp_ether_hdr);		
	free(icmp_ip_hdr);
	free(icmp_packet);
}

sr_ethernet_hdr_t* create_ether_hdr(uint8_t* dhost, uint8_t* shost, uint16_t type){
	sr_ethernet_hdr_t* ether_hdr = (sr_ethernet_hdr_t*)malloc(sizeof(sr_ethernet_hdr_t));
    memcpy(ether_hdr->ether_dhost, dhost, ETHER_ADDR_LEN);  /* destination ethernet address */
    memcpy(ether_hdr->ether_shost, shost, ETHER_ADDR_LEN);  /* source ethernet address */
    ether_hdr->ether_type = htons(type);
	return ether_hdr;
}

struct sr_if* sr_get_interface_with_ip(struct sr_instance* sr, uint32_t ip){
    struct sr_if* if_entry = NULL;
    assert(ip);
    assert(sr);

    if_entry = sr->if_list;
    while(if_entry){
		if(if_entry->ip == ip)
			return if_entry;
        if_entry = if_entry->next;
    }
    return NULL;
} /* -- sr_get_interface -- */

struct sr_rt* get_longest_prefix_match(struct sr_instance * sr, uint32_t ip){
	struct sr_rt* ret_rt_entry = NULL;
	uint32_t max=0;
	struct sr_rt* current_entry = sr->routing_table;
	
	while (current_entry != NULL){
		if ((current_entry->dest.s_addr & current_entry->mask.s_addr) == (ip & current_entry->mask.s_addr) && max <= current_entry->mask.s_addr){
			max = current_entry->mask.s_addr;
			ret_rt_entry = current_entry;
		}
		current_entry=current_entry->next;
	}
	return ret_rt_entry;
}

/*AAA*/
/*Yuan Code Start ============================================================================*/

void sr_nat_handle_ip(struct sr_instance* sr, struct sr_nat *nat, uint8_t * packet, unsigned int len, struct sr_if* in_iface, struct sr_ethernet_hdr* ether_hdr) 
{
    /*extract information from ip header needed to determine if icmp or tcp*/
    printf("\nnow in sr_nat_handle_ip\n");
    struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*)(packet + sizeof(struct sr_ethernet_hdr));
    Debug("\nin sr_nat_handle_ip: before if\n");
    if (ip_hdr->ip_p == ip_protocol_icmp) {
        Debug("\n----------------before nat icmp-------------\n");
        sr_nat_handle_icmp(sr, nat, packet, len, in_iface, ether_hdr);
        Debug("\nin sr_nat_handle_ip: in if\n");
        return;
    }
    else if (ip_hdr->ip_p == ip_protocol_tcp) {
        sr_nat_handle_tcp(sr, nat, packet, len, in_iface, ether_hdr);
        Debug("in sr_nat_handle_ip: in else if");
        return;
    }
    
    else {
        Debug("in sr_nat_handle_ip: before return");
        return;
    }
    return;
}

    void sr_nat_handle_icmp(struct sr_instance* sr, struct sr_nat *nat, uint8_t * packet, unsigned int len, struct sr_if* in_iface, struct sr_ethernet_hdr* ether_hdr) {
        assert(sr);
        assert(nat);
        assert(packet);
        assert(len);
        assert(in_iface);
        Debug("\n===================after assert=====================\n");
        
        /*extract information from ip header needed for processing icmp packet*/
        struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*)(packet + sizeof(struct sr_ethernet_hdr));
        uint32_t src_ip_original = ip_hdr->ip_src;
        uint32_t dst_ip_original = ip_hdr->ip_dst;
        
        /*extract information from icmp header needed for processing icmp packet*/
        /*assumes this is a icmp echo or reply message for now; will recast later in the decision tree if diff type */
        struct sr_icmp_hdr* icmp_hdr= (struct sr_icmp_hdr*)(packet + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr));
        uint8_t icmp_type = icmp_hdr->icmp_type; /*the icmp type is in the same location for icmp echo and icmp type 3*/
        uint16_t icmp_id_original = icmp_hdr->icmp_iden; /*this assumes icmp echo request or reply*/
        
        /*Perform checksum calculations*/
        if (ip_hdr->ip_sum != ip_cksum(ip_hdr)) {
            Debug("ICMP Packet - IP checksum failed; Drop");
            return;
        }
        if (icmp_hdr->icmp_sum != icmp_cksum(ip_hdr, icmp_hdr)) {
            Debug("ICMP Packet - ICMP checksum failed; Drop");
            return;
        }
        Debug("\n*******************after icmp cksum********************\n");
        /*determine if the dest_ip is one of the router's interfaces*/
        struct sr_if* for_router_iface = sr_match_dst_ip_to_iface(sr, ip_hdr);
        
        /*if the dst_ip is not one of the router interfaces or in our routing table*/
        /*call simple router to send icmp t3 msg*/
        struct in_addr dest_ip_ad;
        dest_ip_ad.s_addr = dst_ip_original;
        if(!(for_router_iface)&&!(sr_longest_prefix_match(sr, dest_ip_ad))){
            Debug("ICMP packet - dst ip is not router interface or in routing table; respond with icmp t3 msg - net unreachable");
            /*call simple router*/
            sr_handle_ip(sr, packet, len, in_iface->name);
            return;
        }
        Debug("\n*******************before check_if_internal********************\n");
        if(sr_check_if_internal(in_iface))
        {	
	    Debug("\n----------now in branch 1-------------\n");
            /*if the ICMP packet is from inside NAT*/
            
            /*check if the icmp packet is for router's interface or for inside NAT*/
            /*then call simple router*/
            
	    Debug("--------icmp_type: %d, ICMP_ECHO_REQUEST_TYPE: %d----------------",icmp_type, ICMP_ECHO_REQUEST_TYPE);
	    if((for_router_iface)||sr_check_if_internal(sr_get_outgoing_interface(sr, dst_ip_original)))
            {
                
                /*call simple router*/
                Debug("\nICMP Packet from inside NAT to router interface or to inside NAT - call simple router\n");
                sr_handle_ip(sr, packet, len, in_iface->name);
                return;
                Debug("\nICMP Packet from inside NAT to router interface or to inside NAT - simple router returned\n");
            }
            
            else
            {
                /*the icmp packet is from inside to outside; we will only process echo request
                 if the ICMP packet is an echo request from inside NAT to outside NAT, then we need to do translation*/
		if(icmp_type == ICMP_ECHO_REQUEST_TYPE)
                {
                    /*if ICMP packet is an echo request from inside NAT to outside NAT*/
                    
                    Debug("\n---------------------now in ICMP ECHO REQUEST-----------------------------\n");
                    Debug("\nClient send icmp echo request to outside NAT\n");
                    struct sr_nat_mapping* nat_map = sr_nat_lookup_internal(nat, src_ip_original, icmp_id_original, nat_mapping_icmp);
		    if (nat_map == NULL)
                    {
                        Debug("\n-----------------now in nat_map=NULL---------------\n");
		        struct sr_if * out_iface = sr_get_outgoing_interface(sr, dst_ip_original);
                        nat_map = sr_nat_insert_mapping(nat, src_ip_original, icmp_id_original, nat_mapping_icmp, sr, out_iface->name);
		    printf("\n------------------nat_map: %d----------------\n", nat_map);
                    printf("\n----------in bracket------ip_int: %d, ip_ext: %d, aux_int: %d, aux_ext: %d-------------\n", nat_map->ip_int, nat_map->ip_ext, nat_map->aux_int, nat_map->aux_ext);
                    }
                    printf("\n----------out bracket------ip_int: %d, ip_ext: %d, aux_int: %d, aux_ext: %d-------------\n", nat_map->ip_int, nat_map->ip_ext, nat_map->aux_int, nat_map->aux_ext);
                    ip_hdr->ip_src = nat_map->ip_ext;
                    icmp_hdr->icmp_iden = nat_map->aux_ext;
                    /*update check sums*/
                    ip_hdr->ip_sum = 0;
                    ip_hdr->ip_sum = cksum(ip_hdr, ip_hdr->ip_hl*4);
                    
		    int icmp_offset = sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr);
                    icmp_hdr->icmp_sum = 0;
                    icmp_hdr->icmp_sum = cksum(icmp_hdr, len - icmp_offset);
                    printf("--------------before call simple router-----------");
                    /*call simple router*/
                    sr_handle_ip(sr, packet, len, in_iface->name);
		    printf("--------------after call simple router------------");
                    return;
                }
                else
                {
                    Debug("---------------------now in ICMP ECHO REQUEST-----------------------------");
                    /*if ICMP packet is a type 3 error msg from inside NAT to outside NAT*/
                    struct sr_icmp_t3_hdr* icmp_t3_hdr = (struct sr_icmp_t3_hdr*)(packet + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr));
                    
                    Debug("Client sends type 3 or type 11 icmp message to outside NAT");
                }
            }
        }
        else if (!(sr_check_if_internal(in_iface))) {
            /*if the ICMP packet is from outside NAT
             then we will only handle echo reply and echo request to the external interface of router
             �
             check if the dest_ip not router interface
             then it can be for external host -> simple router; anything else we drop*/
	    Debug("\n----------now in branch 2-------------\n");
            if(!(for_router_iface)){
                if (!(sr_check_if_internal(sr_get_outgoing_interface(sr, dst_ip_original)))){
                    /*call simple router*/
                    sr_handle_ip(sr, packet, len, in_iface->name);
                    return;
                }
                else{
                    /*drop packet*/
                    return;
                }
            }
            else{
                /*the icmp packet is for router interface
                 if it is an echo request, we will echo reply from the router's external interface
                 if it is an echo reply, we will do NAT translate and forward to internal client*/
                if (icmp_type == ICMP_ECHO_REQUEST_TYPE){
                    /*call simple router*/
                    sr_handle_ip(sr, packet, len, in_iface->name);
                    return;
                }
                else if(icmp_type == ICMP_ECHO_REPLY_TYPE){
                    Debug("Client send icmp echo request to outside NAT");
                    
                    struct sr_nat_mapping* nat_map = sr_nat_lookup_external(nat, icmp_id_original, nat_mapping_icmp);
                    if (nat_map == NULL){
                        /*if the icmp echo reply does not match any icmp echo request that we sent*/
                        /*drop*/
                        return;
                    }
                    ip_hdr->ip_dst = nat_map->ip_int;
                    icmp_hdr->icmp_iden = nat_map->aux_int;
                    /*update check sums*/
                    ip_hdr->ip_sum = 0;
                    ip_hdr->ip_sum = cksum(ip_hdr, ip_hdr->ip_hl*4);
                    
                    int icmp_offset = sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr);
                    icmp_hdr->icmp_sum = 0;
                    icmp_hdr->icmp_sum = cksum(icmp_hdr, len - icmp_offset);
                    
                    /*call simple router*/
                    sr_handle_ip(sr, packet, len, in_iface->name);
                    return;
                }
                else
                {
                    Debug("ICMP type 3 or 11 msg from outside NAT to inside NAT");
                }
                
            }
            
            
            
            /*check if the dest_ip is for router
             
             if the dest_ip is not for router or external host, we drop the packet*/
            
        }
        
        else{
            
            
	    Debug("\n----------now in branch 3-------------\n");
            /*call simple router*/
            Debug("Default behavior - call simple router; need to see if this is acceptable");
            sr_handle_ip(sr, packet, len, in_iface->name);
            return;
        }
        
        
        
    }
    
    /*function to return i_face struct of the router if the dest ip is for the i_face of the router*/
    struct sr_if* sr_match_dst_ip_to_iface(struct sr_instance* sr, struct sr_ip_hdr* ip_hdr)
    {
        assert(sr);
        assert(ip_hdr);
        struct sr_if* router_if = sr->if_list;
        
        while(router_if != NULL)
        {
            if (ip_hdr->ip_dst == router_if->ip) /* dest is router*/
            {
                return router_if;
            }
            router_if = router_if->next;
        }
        
        return NULL; /*dest is another.*/
    }
    
    
    /*function to map ip address to interface*/
    struct sr_if* sr_get_outgoing_interface(struct sr_instance* sr, uint32_t ip){
        assert(sr);
        assert(ip);
        
        struct in_addr dest_ip_ad;
        dest_ip_ad.s_addr = ip;
        struct sr_rt* result_route = sr_longest_prefix_match(sr, dest_ip_ad);
        assert(result_route);
        char* out_iface_name = result_route->interface;
	assert(out_iface_name);
        struct sr_if* out_iface = sr_get_interface(sr, out_iface_name);
	assert(out_iface);
        
        return out_iface;
    }
    
    
    /*function to check whether the receiving interface is internal or external*/
    int sr_check_if_internal(struct sr_if* in_iface){
        return (strcmp(in_iface->name, "eth1")==0);
    }
    
    /*Yuan Code Ends ============================================================================*/
    
    
    /*Chenguang Code Start=======================================================================*/
    
    void sr_nat_handle_tcp(struct sr_instance* sr, struct sr_nat *nat, uint8_t * packet, unsigned int len, struct sr_if* in_iface, struct sr_ethernet_hdr* ether_hdr)
    {
        /*extract information from ip header needed for processing icmp packet*/
        struct sr_ip_hdr* ip_hdr = (struct sr_ip_hdr*)(packet + sizeof(struct sr_ethernet_hdr));
        uint32_t src_ip_original = ip_hdr->ip_src;
        
        /*extract information from icmp header needed for processing icmp packet*/
        struct sr_tcp_hdr* tcp_hdr= (struct sr_tcp_hdr*)(packet + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr));
        uint16_t tcp_src_port_original = tcp_hdr->src_port;
        uint16_t tcp_dst_port_original = tcp_hdr->dst_port;
        
        
        /*determine the outgoing interface*/
        uint32_t dst_ip = ip_hdr->ip_dst; /*use ip_dst to determine whether the destination is inside or outside of NAT*/
        struct sr_if* for_router_iface = sr_match_dst_ip_to_iface(sr, ip_hdr);
        
        /*Perform checksum calculations*/
        if (ip_hdr->ip_sum != ip_cksum(ip_hdr)) {
            Debug("ICMP Packet - IP checksum failed; Drop");
            return;
        }
        /*Sanity check on TCP packet*/
        /*We need to check TCP sum now since we will be updating it later*/
        printf("\n--------------tcp_cksum: %d, tcp_sum_original: %d-------------------\n", tcp_cksum(ip_hdr, tcp_hdr, len), tcp_hdr->sum);
        if(tcp_cksum(ip_hdr, tcp_hdr, len) != tcp_hdr->sum){
            Debug("TCP packet received - TCP checksum failed; drop packet");
            return;
        }
        
        
        /*if the dst_ip is not one of the router interfaces or in our routing table*/
        /*call simple router to send icmp t3 msg*/
        struct in_addr dest_ip_ad;
        dest_ip_ad.s_addr = dst_ip;
        if(!(for_router_iface)&&!(sr_longest_prefix_match(sr, dest_ip_ad))){
            Debug("TCP packet received - dst ip is not router interface or in routing table; respond with icmp t3 msg - net unreachable");
            /*call simple router*/
            sr_handle_ip(sr, packet, len, in_iface->name);
            return;
        }
        
        
        if(sr_check_if_internal(in_iface))/*packet is from inside*/
        {
            if((for_router_iface)||sr_check_if_internal(sr_get_outgoing_interface(sr, dst_ip))) /*packet is for router or packet is for inside clients*/
            {
                /*call simple router*/
                sr_handle_ip(sr, packet, len, in_iface->name);
                return;
            }
            else /*packet is going outside*/
            {
                struct sr_nat_mapping* nat_map = sr_nat_lookup_internal(nat, src_ip_original, tcp_src_port_original, nat_mapping_tcp);
                if (nat_map == NULL)
                {
                    struct sr_if * out_iface = sr_get_outgoing_interface(sr, dst_ip);
                    nat_map = sr_nat_insert_mapping(nat, src_ip_original, tcp_src_port_original, nat_mapping_tcp, sr, out_iface->name);
                }
                
                /*lock*/
                pthread_mutex_lock(&((sr->nat).lock));
                
                /*look up tcp connections*/
                struct sr_nat_connection *tcp_con = sr_nat_lookup_tcp_con(nat_map, dst_ip);/*sr_nat_lookup_tcp_con*/
                
                /*if there is no tcp connection, create a tcp connection*/
                if (tcp_con == NULL)
                {
                    tcp_con = sr_nat_insert_tcp_con(nat_map, dst_ip);/*sr_nat_insert_tcp_con*/
                }
                
                switch (tcp_con->tcp_state)
                {
                    case CLOSED:
                        if (ntohl(tcp_hdr->ack_num) == 0 && tcp_hdr->syn && !tcp_hdr->ack)/*sent SYN*/
                        {
                            tcp_con->client_isn = ntohl(tcp_hdr->seq_num);	/*set client ISN*/
                            tcp_con->tcp_state = SYN_SENT;					/*change TCP connection state*/
                        }
                        break;
                        
                    case SYN_RCVD:
                        if (ntohl(tcp_hdr->seq_num) == tcp_con->client_isn + 1 && ntohl(tcp_hdr->ack_num) == tcp_con->server_isn + 1 && !tcp_hdr->syn)/*This is our second packet, sent ACK, for both 3-way and simultaneous open*/
                        {
                            tcp_con->client_isn = ntohl(tcp_hdr->seq_num);	/*not isn, just keep client sequence number, not used*/
                            tcp_con->tcp_state = ESTABLISHED;					/*change TCP connection state*/
                        }
                        break;
                        
                    case ESTABLISHED:
                        if (tcp_hdr->fin && tcp_hdr->ack) /*sent FIN*/
                        {
                            tcp_con->client_isn = ntohl(tcp_hdr->seq_num);   /*not isn, just keep client sequence number, not used*/
                            tcp_con->tcp_state = CLOSED;					   /*change TCP connection state*/
                        }
                        break;
                        
                    default:
                        break;
                }
                tcp_con->last_updated = time(NULL);
                /*unlock*/
                pthread_mutex_unlock(&((sr->nat).lock));
                /* End of critical section. */
                
                
                ip_hdr->ip_src = nat_map->ip_ext;
                tcp_hdr->src_port = htons(nat_map->aux_ext);
                
                /*checksum*/
                ip_hdr->ip_sum = 0;
                ip_hdr->ip_sum = cksum(ip_hdr, ip_hdr->ip_hl*4);
                
                tcp_hdr->sum = tcp_cksum(ip_hdr, tcp_hdr, len);
                assert(tcp_hdr->sum);
                
                /*call simple router*/
                sr_handle_ip(sr, packet, len, in_iface->name);
                return;
            }
        }
        else if(!sr_check_if_internal(in_iface)) /*packet is from outside*/
        {
            if(!(for_router_iface))/*if it's not for router*/
            {
                if (!(sr_check_if_internal(sr_get_outgoing_interface(sr, dst_ip))))/*if its outgoing port is outside, then just use simple router to deal with it*/
                {
                    /*call simple router*/
                    sr_handle_ip(sr, packet, len, in_iface->name);
                    return;
                }
                else/*if it is to inside, drop it*/
                {
                    /*drop packet*/
                    return;
                }
            }
            else /*Inbound packet -> if it is for the router, use NAT*/
            {
                struct sr_nat_mapping *nat_map = sr_nat_lookup_external(&(sr->nat), ntohs(tcp_hdr->dst_port), nat_mapping_tcp);
                if(nat_map == NULL)
                {
                    return;
                }
                else
                {
                    /*lock*/
                    pthread_mutex_lock(&((sr->nat).lock));
                    
                    struct sr_nat_connection *tcp_con = sr_nat_lookup_tcp_con(nat_map, src_ip_original);
                    if (tcp_con == NULL)
                    {
                        tcp_con = sr_nat_insert_tcp_con(nat_map, src_ip_original);
                    }
                    
                    switch (tcp_con->tcp_state)
                    {
                        case SYN_SENT:
                            if (ntohl(tcp_hdr->ack_num) == tcp_con->client_isn + 1 && tcp_hdr->syn && tcp_hdr->ack)
                            {
                                tcp_con->server_isn = ntohl(tcp_hdr->seq_num);
                                tcp_con->tcp_state = SYN_RCVD;
                            }
                            /* Simultaneous open */
                            else if (ntohl(tcp_hdr->ack_num) == 0 && tcp_hdr->syn && !tcp_hdr->ack)
                            {
                                tcp_con->server_isn = ntohl(tcp_hdr->seq_num);
                                tcp_con->tcp_state = SYN_RCVD;
                            }
                            break;
                        default:
                            break;
                    }
                    tcp_con->last_updated = time(NULL);
                    /*unlock*/
                    pthread_mutex_unlock(&((sr->nat).lock));
                    
                    ip_hdr->ip_dst = nat_map->ip_int;
                    tcp_hdr->dst_port = nat_map->aux_int;
                    /*checksum*/
                    ip_hdr->ip_sum = 0;
                    ip_hdr->ip_sum = cksum(ip_hdr, ip_hdr->ip_hl*4);
                    
                    tcp_hdr->sum = tcp_cksum(ip_hdr, tcp_hdr, len);
                    
                    /*call simple router*/
                    sr_handle_ip(sr, packet, len, in_iface->name);
                    return;
                }
            }
            
        }
        
    }
    
    /*
     * calculate TCP checksum
     */
    uint32_t tcp_cksum(struct sr_ip_hdr *ipHdr, struct sr_tcp_hdr *tcpHdr, int total_len)
    {
        
        uint8_t *pseudo_tcp;
        struct sr_tcp_psuedo_hdr *tcp_psuedo_hdr;
        
        int tcp_len = total_len - (sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_ip_hdr));
        int pseudo_tcp_len = sizeof(struct sr_tcp_psuedo_hdr) + tcp_len;
        
        tcp_psuedo_hdr = malloc(sizeof(struct sr_tcp_psuedo_hdr));
        memset(tcp_psuedo_hdr, 0, sizeof(struct sr_tcp_psuedo_hdr));
        
        tcp_psuedo_hdr->ip_src = ipHdr->ip_src;
        tcp_psuedo_hdr->ip_dst = ipHdr->ip_dst;
        tcp_psuedo_hdr->ip_p = ipHdr->ip_p;
        tcp_psuedo_hdr->tcp_len = htons(tcp_len);
        
        uint16_t currCksum = tcpHdr->sum;
        tcpHdr->sum = 0;
        
        pseudo_tcp = malloc(sizeof(struct sr_tcp_psuedo_hdr) + tcp_len);
        memcpy(pseudo_tcp, (uint8_t *) tcp_psuedo_hdr, sizeof(struct sr_tcp_psuedo_hdr));
        memcpy(&(pseudo_tcp[sizeof(struct sr_tcp_psuedo_hdr)]), (uint8_t *) tcpHdr, tcp_len);
        tcpHdr->sum = currCksum;
        
        uint16_t calcCksum = cksum(pseudo_tcp, pseudo_tcp_len);
        
        /* Clear out memory used for creation of complete tcp packet */
        free(tcp_psuedo_hdr);
        free(pseudo_tcp);
        
        return calcCksum;
    }
    
    
    int ip_cksum(struct sr_ip_hdr* ip_hdr) {
        /*IP checksum*/
        uint16_t rcv_cksum = ip_hdr->ip_sum;
        ip_hdr->ip_sum = 0;
        /*New*/
        /*uint16_t cal_cksum = cksum(ip_hdr, sizeof(struct sr_ip_hdr));*/
        uint16_t cal_cksum = cksum(ip_hdr, ip_hdr->ip_hl * 4);
        /*reset check sum*/
        ip_hdr->ip_sum = rcv_cksum;
        
        return cal_cksum;
    }
    
    int icmp_cksum(struct sr_ip_hdr* ip_hdr, struct sr_icmp_hdr* icmp_hdr) {
        uint16_t icmp_expected_cksum;
        uint16_t icmp_received_cksum;
        
        icmp_received_cksum = icmp_hdr->icmp_sum;
        icmp_hdr->icmp_sum = 0;
        icmp_expected_cksum = cksum(icmp_hdr, ntohs(ip_hdr->ip_len) - ip_hdr->ip_hl * 4);
        
        /*reset check sum*/
        icmp_hdr->icmp_sum = icmp_received_cksum;
        
        return icmp_expected_cksum;
        /*ICMP checksum end*/
    }
    
/*
 * perform longest prefix match
 */
struct sr_rt *sr_longest_prefix_match(struct sr_instance* sr, struct in_addr addr)
{
    struct sr_rt* lpm = NULL;
    uint32_t lpm_len = 0;
    struct sr_rt* rt = sr->routing_table;
  
    while( rt != 0 ) 
    {
        if (((rt->dest.s_addr & rt->mask.s_addr) == (addr.s_addr & rt->mask.s_addr)) &&
              (lpm_len <= rt->mask.s_addr)) 
        {
              
            lpm_len = rt->mask.s_addr;
            lpm = rt;
        }
        
        rt = rt->next;
    }
    
    return lpm;
}
    
    /*Chenguang Code End=========================================================================*/
